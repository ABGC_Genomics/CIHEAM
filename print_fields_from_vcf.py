############################
# H.J. Megens, 07/03/2015
# For description, see CIHEAM tutorial
############################
import argparse
import sys

parser = argparse.ArgumentParser( description='prints a selection of fields from a vcf file')
parser.add_argument("-m", "--method", help="annotation method", type=str, choices=['none','vep','snpeff'], default='none')

def vcfelements(line): # parse the main, tab-delimited elements of the VCF line
   vcf_elements=line.split('\t')
   vcf_elements[7]=tagelements(vcf_elements[7])
   if len(vcf_elements)>9: # check if genotype fields present; this requires at least 2 additional columns
      vcf_elements=list_genotypes(vcf_elements)
   else :
      vcf_elements.append([])  # make sure that the data structure is allways of length 9
   return vcf_elements # return list

def list_genotypes(vcflist): # reshape the genotypes to a single list
   genotypes=vcflist[9:]
   gt_field=list()
   gt_field.append(vcflist[8])
   gt_field.append(genotypes)
   vcflist=vcflist[0:8]
   vcflist.append(gt_field)
   return vcflist

def tagelements(tag): # parse the fields/tags in the 8th column - ';' delimited
   tags=tag.split(';')
   tagdict={tag.split('=')[0]:tag.split('=')[1] for tag in tags}
   if 'CSQ' in tagdict.keys(): # VEP annotation has tag 'CSQ'
      tagdict['CSQ']=vepelements(tagdict['CSQ'])
   elif 'ANN' in tagdict.keys(): # snpEff annotation has tag 'ANN'
      tagdict['ANN']=vepelements(tagdict['ANN'])
   return tagdict # return dict

def snpeffelements(snpeff): # parse the snpEff specific annotation - '|' delimited
   snpeff_elements=vep.split(',')[0].split('|')
   return snpeff_elements # return list

def vepelements(vep): # parse the VEP specific annotation - '|' delimited
   vep_elements=vep.split(',')[0].split('|')
   if len(vep_elements)==15:
      if vep_elements[14]:
         vep_elements[14]=siftelements(vep_elements[14])
   return vep_elements # return list

def siftelements(sift): # parse the sift prediction and score from the VEP annotation
   sift_elements=sift.split('(')
   sift_elements[1]=sift_elements[1].replace(')','')
   return sift_elements # return list

def print_by_method(vcf_line,method): # VEP and snpEff specific printing of fields
   chrom=vcf_line[0] 
   coord=vcf_line[1]
   af='-9'
   if 'AF' in vcf_line[7].keys(): # check if 'AF' tag present in dict
      af=vcf_line[7]['AF']
   print(chrom,coord,af,sep='\t', end='\t') # fields that are in common
   if method == 'none' or ('CSQ' not in vcf_line[7].keys() and 'ANN' not in vcf_line[7].keys()):
      print('')
   elif method == 'vep': # check if VEP specific fields to be printed
      consequence=vcf_line[7]['CSQ'][4]
      gene=vcf_line[7]['CSQ'][1] if vcf_line[7]['CSQ'][1] else '-9'
      sift_prediction='-9'
      sift_score='-9'
      if vcf_line[7]['CSQ'][14]:
        sift_prediction=vcf_line[7]['CSQ'][14][0]
        sift_score=vcf_line[7]['CSQ'][14][1]
      print(gene,consequence,sift_prediction, sift_score,sep='\t')
   elif method == 'snpeff': # check if snpEff specific fields to be printed
      consequence=vcf_line[7]['ANN'][1]
      gene='intergenic' if vcf_line[7]['ANN'][1] == 'intergenic_region' else vcf_line[7]['ANN'][4]
      snpeff_prediction= vcf_line[7]['ANN'][2]
      print(gene,consequence,snpeff_prediction,sep='\t')

def _void_f(*args,**kwargs): # a void to make the uncaught exception disappear in P3.4
    pass

if __name__=="__main__":
   args = parser.parse_args()
   method=args.method # parse arguments
   try:
      for myline in sys.stdin.readlines(): # read from STDIN
         myline=myline[:-1]
         if not myline.startswith('#'): # avoid VCF comment lines
            vcf_line=vcfelements(myline)
            #print(vcf_line)
            print_by_method(vcf_line,method)
   except (BrokenPipeError,IOError): # exception handling for BrokenPipeError; works in P3.3, not P3.4! Need this workaround
     sys.stdout.write = _void_f
     sys.stdout.flush = _void_f
     sys.exit()
  

##Just another demonstration of the power of shell scripting: This little script will display the 40 most uased words across all the collected works of Shakespeare in seconds!
## Inspired by the book "Classic Shell Scripting, Robbins & Beebe, O'Reilly Media

mkdir Shakespeare; 
cd Shakespeare

wget http://sydney.edu.au/engineering/it/~matty/Shakespeare/shakespeare.tar.gz
tar -xzvf shakespeare.tar.gz

cat */* | tr -cs A-Za-z\' '\n' | tr A-Z a-z | sort | uniq -c | sort -k1,1nr -k2 | head -40 | pr -c4 -t -w80
echo '----'

cat */* | tr -cs A-Za-z\' '\n' | sed 's/\(.*\)/\L\1/' | sort | uniq -c | sort -k1,1nr -k2 | head -40 | pr -c4 -t -w80
echo '----'

cat */* | tr -cs A-Za-z\' '\n' | perl -p -i -e 'tr/A-Z/a-z/' | sort | uniq -c | sort -k1,1nr -k2 | head -40 | pr -c4 -t -w80
echo '----'

cat */* | tr -cs A-Za-z\' '\n' | python3 ../thepythonway.py | sort | uniq -c | sort -k1,1nr -k2 | head -40 | pr -c4 -t -w80
